# WEB API TEST AUTOMATION BY MARCO DE JESUS

The following project is created to quickly summarize a simple CRUD Process for the petStore Interface.

[PETSTORE](https://petstore.swagger.io/)

### ASSUMPTIONS
- Postman scripts can run under a collection runner and have the most basic test. There is dynamic variables, nor json body response evaluation.
- As Web API is using different timing on the response, these test cases are prone to fail. 
- Thread.sleep has been utilized, knowing that it's not the best practice for this.
- Endpoints and base url, could have a better treatment under enum class.

## Technology involved
* Spring Boot
* Maven
* Junit 5
* Rest Template 
* Jackson
* SLF4J
* Postman

The postman file could be found under "src/main/resources/postman". 

## Installation

Clone this repostory

```bash
git clone git@gitlab.com:marcodejesus2010/pet-store-web-api-automation-testing.git
```

## Usage

For any environment
```maven
mvn test
```
or; to work with the unix executable for maven wrapper
```maven
./mvnw test
```
or; to work with the windows executable for maven wrapper
```maven
./mvnw.cmd test
```
Or run from the Run functionality that comes from the IDE.
