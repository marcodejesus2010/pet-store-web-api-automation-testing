package com.mdejesus.petstore.automatepetstore;

import com.mdejesus.petstore.automatepetstore.models.Category;
import com.mdejesus.petstore.automatepetstore.models.Pet;
import com.mdejesus.petstore.automatepetstore.models.Tag;
import org.junit.jupiter.api.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertThrows;

@SpringBootTest
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class AutomatePetstoreApplicationTests {

    Logger logger = LoggerFactory.getLogger(AutomatePetstoreApplicationTests.class);

    final String baseUrl = "https://petstore.swagger.io/v2";
    static final int categoryId = 23;
    static final int tagId = 453;
    static int petId;
    static Pet tommyDoggie;

    // NOT THE BEST PRACTICE THOUGH
    int timer = 7000;

    RestTemplate template = new RestTemplate();
    ResponseEntity<Pet> response;

    @BeforeAll
    public static void setUp() {
        petId = 3300;
        tommyDoggie = new Pet()
                .withId(petId)
                .withName("Tomas")
                .withStatus("available")
                .withCategory(new Category()
                        .withId(categoryId)
                        .withName("Cutties"))
                .withPhotoUrls(new ArrayList<>() {{
                    add("photoKerbero.jpg");
                }})
                .withTags(new ArrayList<>() {{
                    add(new Tag()
                            .withId(tagId)
                            .withName("Kerberos"));
                }});
    }

    @Test
    @Order(0)
    void GivenAPetInfo_WhenAddingToTheStore_ThenPetIsAvailableForSale() {
        logger.info("Creating a new PET with ID: " + petId);
        var expectedStatusCode = 200;
        var expectedName = "Tomas";

        response = template.postForEntity(baseUrl + "/pet", tommyDoggie, Pet.class);
        var actualStatusCode = response.getStatusCodeValue();
        var actualPetName = response.getBody().getName();

        Assertions.assertEquals(expectedStatusCode, actualStatusCode, "Pet has not been placed into the Store.");
        Assertions.assertTrue(actualPetName.equals(expectedName), "Pet Name is not right.");
    }

    @Test
    @Order(1)
    void GivenAPetId_WhenLookingItForItsId_ThenPetIsFound() throws InterruptedException {
        logger.info("SEARCHING FOR PET ID: " + petId);
        Thread.sleep(timer);
        var expectedStatusCode = 200;

        response = template.getForEntity(baseUrl + "/pet/" + petId, Pet.class);
        var actualStatusCode = response.getStatusCodeValue();

        Assertions.assertEquals(expectedStatusCode, actualStatusCode, "Pet has not been found.");
    }

    @Test
    @Order(2)
    void GivenAnExistingPet_WhenUpdatingItsName_ThenNameIsUpdated() throws InterruptedException {
        var expectedPetName = "Kerberos";
        logger.info("UPDATING PET NAME to: " + expectedPetName);
        tommyDoggie.withName("Kerberos");

        template.put(baseUrl + "/pet", tommyDoggie);
        Thread.sleep(timer);
        response = template.getForEntity(baseUrl + "/pet/" + petId, Pet.class);
        var actualPetName = response.getBody().getName();

        Assertions.assertEquals(expectedPetName, actualPetName, "The name of Pet has not changed.");
    }

    @Test
    @Order(3)
    void GivenAPetID_WhenCallingDeleteMethod_ThenPetSearchReturns404() throws InterruptedException {
        logger.info("PET with ID " + petId + " is being deleted.");
        var expectedMessage = "404 Not Found";

        template.delete(baseUrl + "/pet/" + petId);
        Thread.sleep(timer);
        Exception exception = assertThrows(RuntimeException.class,
                () -> template.getForEntity(baseUrl + "/pet/" + petId, Pet.class));
        var actualMessage = exception.getMessage();

        Assertions.assertTrue(actualMessage.contains(expectedMessage), "Pet has not been deleted.");
    }
}
