package com.mdejesus.petstore.automatepetstore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutomatePetstoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(AutomatePetstoreApplication.class, args);
	}

}
